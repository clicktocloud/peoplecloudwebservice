package com.clicktocloud.jobpostingws.Exception;

public class JobPostingException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JobPostingException() {
		// TODO Auto-generated constructor stub
	}

	public JobPostingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JobPostingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public JobPostingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
