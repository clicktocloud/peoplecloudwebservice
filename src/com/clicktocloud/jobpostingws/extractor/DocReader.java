package com.clicktocloud.jobpostingws.extractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.log4j.Logger;
import org.apache.poi.POITextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

/*
 * this class extract doc, docx, pdf, content into txt file
 */
public class DocReader {

	
	static Logger logger = Logger
			.getLogger("com.clicktocloud.jobapply.extrator.DocReader");
	
	public static final String CLASSNAME = "com.clicktocloud.jobapply.extrator";
	public static final String WARNING = "Failed to parse the document. Please check it contents.";
	private File inputfile;

	public DocReader() {
	}

	/*
	 * assign the path of input file and output file
	 */
	public DocReader(File inputfile) {
		this.inputfile = inputfile;
	}

	/*
	 * according to different file extension to choose different extractor
	 */
	public String chooseReader() {
		String temp = inputfile.toString().toLowerCase();
		if (temp.endsWith(".pdf")) {
			return pdfReader();
		} else if (temp.endsWith(".doc")) {
			return docReader();
		} else if (temp.endsWith(".docx")) {
			return docxReader();
		} else if (temp.endsWith(".rtf")) {
			return rtfReader();
		} else if (temp.endsWith(".txt")) {
			return txtReader();
		} else {
			logger.error("chooseReader : this file format do not support. " + temp);
//			throw new FileParseException("this file format do not support. "
//					+ temp);
			return null;
		}
	}

	/*
	 * extract the pdf content
	 */

	private String pdfReader() {
		logger.debug("in pdfReader");
		String content = "";
		PDDocument pdd = null;
		try {
			
			PDFTextStripper stripper = new PDFTextStripper();
			pdd = PDDocument.load(inputfile);
			String temp = stripper.getText(pdd);
			content = getClean(temp);
			logger.debug("the content is "+content);
			pdd.close();
			
		} catch (Exception ex) {
			content = WARNING;
			logger.debug("enter the Exception of pdfReader");
			if (pdd != null) {
				try {
					pdd.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
			
			content = pdfReader2();
			logger.info("content = "+content);
			return content;
		}
		
		return content;
	}
  
	private String pdfReader2() {
		logger.debug("in pdfReader2");
		StringBuffer sb = new StringBuffer();
		String content = "";
		try {
			String path = inputfile.toString();
			PdfReader reader = new PdfReader(path);
			int numberOfPages = reader.getNumberOfPages();
			PdfTextExtractor extractor = new PdfTextExtractor(reader);
			for (int i = 0; i < numberOfPages; i++) {
				content = extractor.getTextFromPage(i + 1);
				sb.append(content);
			}
			content = sb.toString();
			logger.debug("the content is "+content);
		} catch (Exception ex) {
			printError(inputfile);
			content = WARNING;
			logger.error("cannot parse pdf file : " + inputfile.toString(), ex);
//			throw new FileParseException("cannot parse pdf file : "
//					+ inputfile.toString(), ex);
			return content;
		}
		return content;
	}

	/*
	 * extract the doc content
	 */

	private String docReader() {
		String content = "";
		InputStream is = null;
		try {
			is = new FileInputStream(inputfile);
			// HWPFDocument doc = new HWPFDocument(is);
			// content = doc.getRange().text();
			WordExtractor extractor = new WordExtractor(is);
			String temp = extractor.getText();
			content = getClean(temp);
			is.close();
		} catch (IOException ex) {
			content = WARNING;
			printError(inputfile);
			logger.error("cannot parse doc file : " + inputfile.toString(), ex);
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
//			throw new FileParseException("cannot parse doc file : "
//					+ inputfile.toString(), ex);
			return content;
		}
		return content;
	}

	private String docxReader() {
		logger.debug("in docxReader");
		String content = "";
		try {
			logger.debug("start to extract");
			POITextExtractor extractor = ExtractorFactory.createExtractor(inputfile);
			logger.debug("get extractor");
			String temp = extractor.getText();
			logger.debug("finish extractor");
			logger.debug("the temp is: "+temp);
			content = getClean(temp);
			logger.debug("the content is: "+ content);
		} catch (Exception e) {
			logger.debug("enter the exception");
			content = WARNING;
			printError(inputfile);
			logger.error("cannot parse docx file : " + inputfile.toString(), e);
//			throw new FileParseException("cannot parse docx file : "
//					+ inputfile.toString(), e);
			return content;

		}
		return content;
	}

	private String rtfReader() {
		String content = null;
		InputStream is = null;
		try {
			is = new FileInputStream(inputfile);
			RTFEditorKit aKit = new RTFEditorKit();
			Document aDocument = aKit.createDefaultDocument();
			aKit.read(is, aDocument, 0);
			String temp = aDocument.getText(0, aDocument.getLength());
			content = getClean(temp);
			is.close();
		} catch (Exception e) {
			content = WARNING;
			if (is != null) {
				try {
					is.close();
				} catch (IOException ex) {
				}
			}
			logger.error("cannot parse rtf file : " + inputfile.toString(), e);
			printError(inputfile);
//			throw new FileParseException("cannot parse rtf file : "
//					+ inputfile.toString(), e);
			return content;
		}
		return content;
	}

	/*
	 * extract txt content
	 */
	private String txtReader() {
		StringBuffer content = new StringBuffer();
		BufferedReader br = null;
		String result = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(
					inputfile)));
			String temp = null;
			while ((temp = br.readLine()) != null) {
				content.append(temp);
			}
			temp = content.toString();
			result = getClean(temp);
			return result;
		} catch (IOException ex) {
			result = WARNING;
			printError(inputfile);
			logger.error("cannot parse txt file : " + inputfile.toString(), ex);
			
//			throw new FileParseException("cannot parse txt file : "
//					+ inputfile.toString(), ex);
			return result;
		}finally{
			if (br != null) {
				try {
					br.close();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public File getInputfile() {
		return inputfile;
	}

	public void setInputfile(File inputfile) {
		this.inputfile = inputfile;
	}

	private String getClean(String content) {
		return content
				.replaceAll("[\\x00-\\x1F\\x7F-\\xFF&&[^\\t\\n\\r]]", " ");
	}

	private void printError(File inputfile) {
		logger.error(inputfile.toString());
	}
}
