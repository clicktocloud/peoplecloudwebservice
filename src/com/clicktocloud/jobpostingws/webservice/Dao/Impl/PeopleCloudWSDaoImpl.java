package com.clicktocloud.jobpostingws.webservice.Dao.Impl;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.net.ssl.HttpsURLConnection;
import javax.sql.DataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.clicktocloud.jobpostingws.Exception.JobPostingException;
import com.clicktocloud.jobpostingws.extractor.DocReader;
import com.clicktocloud.jobpostingws.util.GetInformationPorpertiesValue;
import com.clicktocloud.jobpostingws.webservice.Dao.PeopleCloudWSDao;

@WebService( endpointInterface = "com.clicktocloud.jobpostingws.webservice.Dao.PeopleCloudWSDao", targetNamespace = "http://www.clicktocloud.com/peoplecloudws", serviceName = "PeopleCloudWS")
public class PeopleCloudWSDaoImpl implements PeopleCloudWSDao {
	static Logger logger2 = Logger.getLogger(PeopleCloudWSDaoImpl.class);
	public static final String CLASSNAME = "com.clicktocloud.jobpostingws.webservice.Dao.Impl.PeopleCloudWSDaoImpl";
	
	public static String S3_SERVIER_DOMAIN = ".s3.amazonaws.com";
	
	
	@Override
	public boolean transfer(String type, String prefixFilename, String ftype,
			String content) throws Exception {
		boolean result = false;
		String path = "";
		String divpath = ""; 
		String imagepath = "";
		divpath = GetInformationPorpertiesValue
				.getValueFromPropertyFile("divfolder", "information.properties")
				.replace("${catalina.base}", System.getProperty("catalina.base"));
		imagepath = GetInformationPorpertiesValue
				.getValueFromPropertyFile("imgfolder", "information.properties")
				.replace("${catalina.base}", System.getProperty("catalina.base"));
		
		String filename = prefixFilename + "." + ftype;
		if (type.toLowerCase().equals("header")) {
			 //path = "d:\\root\\image\\"+filename;
			// path = "/usr/local/apache2/htdocs/peoplecloud/image/" + filename;
			 path = imagepath + filename;
		}
		if (type.toLowerCase().equals("footer")) {
			// path = "d:\\root\\image\\"+filename;
			//path = "/usr/local/apache2/htdocs/peoplecloud/image/" + filename;
			path = imagepath + filename;
		}
		if (type.toLowerCase().equals("div")) {
			 //path = "d:\\root\\div\\"+filename;
			//path = "/usr/local/apache2/htdocs/peoplecloud/div/" + filename;
			path = divpath + filename;
		}

		//byte[] decodedContent = Base64.decodeBase64(content);
		Base64 base64 = new Base64();
		byte[] decodedContent = base64.decode(content.getBytes());
		BufferedOutputStream bos = null;
//		String temp = new String(decodedContent);
		try {
			bos = new BufferedOutputStream(new FileOutputStream(new File(path)));
			bos.write(decodedContent);
			bos.close();
		} catch (FileNotFoundException e) {
			logger2.error("FileNotFoundException", e);
			throw new JobPostingException("FileNotFoundException:", e);
		} catch (IOException e) {
			logger2.error("IOException", e);
			throw new JobPostingException("IOException:", e);
		}
		result = true;
		return result;
	}


	@Override
	public boolean insertJobPostingDetail(String jobRefCode, String iid,
			String vid, String bucketname, String hname, String htype,
			String dname, String dtype, String fname, String ftype, String divisionName, String website, String jobTitle) {
		boolean result = false;
		
		logger2.debug("start to insert JobPostingDetail xml string");
		Context ctx = null;
		Connection conn = null;
		PreparedStatement pst = null;		
		try{	
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/test001");
			conn = ds.getConnection();
			pst = conn.prepareStatement("insert into jobpostingdetail(jobRefCode, iid, vid, bucketname, hname, htype, dname, dtype, fname, ftype, divisionName, website, jobTitle, milltime, postDate) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			Date date = new Date();
			pst.setString(1, jobRefCode);
			pst.setString(2, iid);
			pst.setString(3, vid);
			pst.setString(4, bucketname);
			pst.setString(5, hname);
			pst.setString(6, htype);
			pst.setString(7, dname);
			pst.setString(8, dtype);
			pst.setString(9, fname);
			pst.setString(10, ftype);
			pst.setString(11, divisionName);
			pst.setString(12, website);
			pst.setString(13, jobTitle);
			pst.setLong(14, date.getTime());
			pst.setTimestamp(15, new Timestamp(date.getTime()));
			pst.execute();
			pst.close();
			ctx.close();
			conn.close();
			result = true;
			logger2.debug("insert mycareer xml string successfully");
		} catch (NamingException e) {
			logger2.error("error in jndi look up: ", e);
			if(conn!=null){try{conn.close();}catch(Exception ex){}}
			if(ctx!=null){try{ctx.close();}catch(Exception ex){}}
			if(pst!=null){try{pst.close();}catch(Exception ex){}}
			throw new JobPostingException("error in jndi look up:", e);
		} catch (SQLException e) {
			logger2.error("error in peopleclouddetail SQL : ", e);
			if(conn!=null){try{conn.close();}catch(Exception ex){}}
			if(ctx!=null){try{ctx.close();}catch(Exception ex){}}
			if(pst!=null){try{pst.close();}catch(Exception ex){}}
			throw new JobPostingException("error in peopleclouddetail SQL enquiry : ", e);
		}
		return result;
	}

	@Override
	public int updateJobPostingByDivisionName(String divisionName,
			String hname, String htype, String dname, String dtype,
			String fname, String ftype, String iid) {
		
		return updateJobPostingDetail("divisionname", hname, htype, dname, dtype, fname, ftype, divisionName, iid,"");
	}
	
	@Override
	public int updateJobPostingWithTitleByDivisionName(String divisionName,
			String hname, String htype, String dname, String dtype,
			String fname, String ftype, String iid, String jobTitle) {
		
		return updateJobPostingDetail("divisionname", hname, htype, dname, dtype, fname, ftype, divisionName, iid,jobTitle);
	}
	
	private int updateJobPostingDetail(String type, String hname,
			String htype, String dname, String dtype, String fname, String ftype, String condition, String iid, String jobTitle){
		
		String sql = "";
		String jobTitleFieldValue = "";
		if(! StringUtils.isEmpty(jobTitle)){
			jobTitleFieldValue = ", jobTitle=? ";
		}
		
		if(type.toLowerCase().equals("divisionname")){
			sql = "update jobpostingdetail set hname=?, htype=?, dname=?, dtype=?, fname=?, ftype=?" + jobTitleFieldValue + " where iid=? and divisionName=?";
		}else if(type.toLowerCase().equals("vid")){
			sql = "update jobpostingdetail set hname=?, htype=?, dname=?, dtype=?, fname=?, ftype=?" + jobTitleFieldValue + " where iid=? and vid=?";
		}else{
			logger2.debug("not such type in jobPostingDetails");
		}
		
		logger2.debug("start to update JobPostingDetail by "+type);
		Context ctx = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int number = 0;
		try{	
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/test001");
			conn = ds.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, hname);
			pst.setString(2, htype);
			pst.setString(3, dname);
			pst.setString(4, dtype);
			pst.setString(5, fname);
			pst.setString(6, ftype);
			if( StringUtils.isEmpty( jobTitleFieldValue )){
				pst.setString(7, iid);
				pst.setString(8, condition);
			}else{
				pst.setString(7, jobTitle);
				pst.setString(8, iid);
				pst.setString(9, condition);
			}
			
			number = pst.executeUpdate();
			pst.execute();
			pst.close();
			ctx.close();
			conn.close();
			
			logger2.debug("update JobPostingDetail by "+type);
			
		} catch (NamingException e) {
			logger2.error("error in jndi look up: ", e);
			
			throw new JobPostingException("error in jndi look up:", e);
		} catch (SQLException e) {
			logger2.error("error in update JobPostingDetail SQL by "+type, e);
			
			throw new JobPostingException("error in update JobPostingDetail SQL  by "+type, e);
		}finally{
			if(conn!=null){try{conn.close();}catch(Exception ex){}}
			if(ctx!=null){try{ctx.close();}catch(Exception ex){}}
			if(pst!=null){try{pst.close();}catch(Exception ex){}}
		}
		return number;
	}
	
	@Override
	public int updateJobPostingByVid(String vid, String hname,
			String htype, String dname, String dtype, String fname, String ftype, String iid) {
		
		return this.updateJobPostingDetail("vid", hname, htype, dname, dtype, fname, ftype, vid, iid,"");

	}
	
	@Override
	public int updateJobPostingWithTitleByVid(String vid, String hname,
			String htype, String dname, String dtype, String fname, String ftype, String iid,String jobTitle) {
		
		return this.updateJobPostingDetail("vid", hname, htype, dname, dtype, fname, ftype, vid, iid,jobTitle);

	}

//	@Override
//	public int deleteJobFromDetailByAID(String iid, String[] aid) {
//		logger2.debug("start to delete Job from detail table");
//		Context ctx = null;
//		Connection conn = null;
//		PreparedStatement pst = null;
//		int[] result = null;
//		int rows = 0;
//		try{
//			
//			synchronized (lock) {
//				ctx = new InitialContext();
//				DataSource ds = (DataSource) ctx
//						.lookup("java:comp/env/jdbc/test001");
//				conn = ds.getConnection();
//			}
//			pst = conn.prepareStatement("delete from jobpostingdetail where iid=? and vid =?");
//			for(String s:aid){
//				pst.setString(1, iid);
//				pst.setString(2, s);
//				pst.addBatch();
//			}
//			result  = pst.executeBatch();
//			
//			if(result!=null){
//				for(int i=0; i<result.length; i++){
//					rows += i;
//				}
//			}
//			pst.close();
//			ctx.close();
//			conn.close();
//			
//			logger2.debug("idelete Job from detail table successfully");
//		} catch (NamingException e) {
//			logger2.error("error in jndi look up: ", e);
//			if(conn!=null){try{conn.close();}catch(Exception ex){}}
//			if(ctx!=null){try{ctx.close();}catch(Exception ex){}}
//			if(pst!=null){try{pst.close();}catch(Exception ex){}}
//			throw new JobPostingException("error in jndi look up:", e);
//		} catch (SQLException e) {
//			logger2.error("error in peopleclouddetail SQL : ", e);
//			if(conn!=null){try{conn.close();}catch(Exception ex){}}
//			if(ctx!=null){try{ctx.close();}catch(Exception ex){}}
//			if(pst!=null){try{pst.close();}catch(Exception ex){}}
//			throw new JobPostingException("SQL error in delete Job from detail table : ", e);
//		}
//		return rows;
//	}

	public static void main(String[] args) throws Exception{
		PeopleCloudWSDaoImpl dao = new PeopleCloudWSDaoImpl();
		String temp = null;
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new FileReader(new File("/home/jack/Desktop/logo.png")));
		while((temp=br.readLine())!=null){
			sb.append(temp);
		}
		br.close();
		String temp2 = dao.post2C1(sb.toString());
//		boolean temp2 = dao.transfer("header", "logo", "png", "test function");
		System.out.println(temp2);
	}

	@Override
	public String post2C1(String jobContent) {
		String result =  "";
		PrintWriter pw = null;
		InputStream is = null;
		try {
			URL url = new URL("https://gateway.monster.com:8443/bgwBroker");
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("Content-Type","text/xml");
			
			pw = new PrintWriter(conn.getOutputStream());
			pw.print(jobContent);
			pw.flush();
			pw.close();
			
			is = conn.getInputStream();
			result = IOUtils.toString(is);
			is.close();
		} catch (MalformedURLException e) {
			logger2.error("MalformedURLException in post2C1 "+ e);
			if(pw!=null){try{pw.close();}catch(Exception ex){}}
			if(is!=null){try{is.close();}catch(Exception ex){}}
		} catch (ProtocolException e) {
			logger2.error("ProtocolException in post2C1 "+ e);
			if(pw!=null){try{pw.close();}catch(Exception ex){}}
			if(is!=null){try{is.close();}catch(Exception ex){}}
		} catch (FileNotFoundException e) {
			logger2.error("FileNotFoundException in post2C1 "+ e);
			if(pw!=null){try{pw.close();}catch(Exception ex){}}
			if(is!=null){try{is.close();}catch(Exception ex){}}
		} catch (IOException e) {
			logger2.error("IOException in post2C1 "+ e);
			if(pw!=null){try{pw.close();}catch(Exception ex){}}
			if(is!=null){try{is.close();}catch(Exception ex){}}
		}
		return result;
	}
	
	/*
	 * Deprecated - not be invoked by client (salesforce)
	 * Commented by Andy Yang 21/04/2017
	 * 
	@Override
	public String sayHello(){
		return "Hello ClicktoCloud";
	}
	*/
	@Override
	public String getContent(String url) {
		// url must include S3_SERVIER_DOMAIN
		if(url == null || !url.contains(S3_SERVIER_DOMAIN)){
			return "";
		}
		
		File file = downloadFile(url);
		String result = transfer2txt(file);
		return result;
	}
	

	 private String transfer2txt(File uploadedFile ){
		
	    DocReader reader = new DocReader(uploadedFile);
	    String content = null;
		String contentTemp = reader.chooseReader();
		content = contentTemp.replaceAll("[\\x00-\\x1F\\x7F-\\xFF&&[^\\t\\n\\r]]", " ");
		return  content;
	 }
	
	private static File downloadFile(String url){
		
		
		int index = StringUtils.lastIndexOf(url, "/");
		String fileName = StringUtils.substring(url, index+1);
		
		String tempFolder = System.getProperty("catalina.base");
		if(tempFolder == null || tempFolder.equals("")){
			tempFolder = "/home/temp001/";
		}else{
			tempFolder = tempFolder + "/temp/";
		}
		
		File desc = new File(tempFolder+fileName);
		URL src;
		try {
			src = new URL(url);
			FileUtils.copyURLToFile(src, desc);
		} catch (MalformedURLException e) {
			logger2.error("MalformedURLException", e);
		} catch (IOException e) {
			logger2.error("IOException", e);
		}
		
		return desc;
	}	
}
