package com.clicktocloud.jobpostingws.webservice.Dao;

import javax.jws.WebService;

@WebService
public interface PeopleCloudWSDao {
	public boolean transfer(String type, String prefixFilename, String ftype, String content) throws Exception;
	
	public boolean insertJobPostingDetail(String jobRefCode, String iid, 
			String vid, String bucketname, String hname, String htype, String dname, 
			String dtype, String fname, String ftype, String divisionName, String website, String jobTitle);
	
	public int updateJobPostingByVid(String vid, String hname, String htype, String dname, 
			String dtype, String fname, String ftype, String iid);
	public int updateJobPostingWithTitleByVid(String vid, String hname, String htype, String dname, 
			String dtype, String fname, String ftype, String iid,String jobTitle);
	
	public int updateJobPostingByDivisionName(String divisionName, String hname, String htype, 
			String dname, String dtype, String fname, String ftype, String iid);
	
	public int updateJobPostingWithTitleByDivisionName(String divisionName,
			String hname, String htype, String dname, String dtype,
			String fname, String ftype, String iid, String jobTitle);
	
//	public int deleteJobFromDetailByAID(String iid, String[] aids);
	
	public String post2C1(String jobContent);
	
	//Commented by andy yang 21/04/2017
	//public String sayHello();
	
	public String getContent(String fileString);
}
