package com.clicktocloud.jobpostingws.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class GetInformationPorpertiesValue {
	static Logger logger = Logger.getLogger("com.clicktocloud.jobpostingws.extractor");
	
	public static String getValueFromPropertyFile(String key,String filename){
		Properties p = new Properties();
		String value = null;
		InputStream in = null;
		try {
			in = GetInformationPorpertiesValue.class.getClassLoader().getResourceAsStream(filename);
			p.load(in);
			value = p.getProperty(key);
			//logger.debug("value of "+key + ":"+ value);
		} catch (IOException e) {
			logger.error("IOException :", e);
		}finally{
			if(in!=null)try{in.close();}catch(Exception e){}
		}
		return value;
	} 
}
